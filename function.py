'''
Exercise1: Encoding the Board
# >>> from utils import display
# >>> display(grid_values('..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..'))
. . 3 |. 2 . |6 . .
9 . . |3 . 5 |. . 1
. . 1 |8 . 6 |4 . .
------+------+------
. . 8 |1 . 2 |9 . .
7 . . |. . . |. . 8
. . 6 |7 . 8 |2 . .
------+------+------
. . 2 |6 . 9 |5 . .
8 . . |2 . 3 |. . 9
. . 5 |. 1 . |3 . .
'''

# 1. utils.py ----------------------------
# 1.1 define rows:
rows = 'ABCDEFGHI'

# 1.2 define cols:
cols = '123456789'


# 1.3 cross(a,b) helper function to create boxes, row_units, column_units, square_units, unitlist
def cross(a, b):
    return [s + t for s in a for t in b]

# 1.4 create boxes
boxes = cross(rows, cols)

# print(boxes)

# 1.5 create row_units
row_units = [cross(r, cols) for r in rows]

# print(row_units)

# 1.6 create column_units
column_units = [cross(rows, c) for c in cols]

# print(column_units)

# 1.7 create square_units for 9x9 squares
square_units = [cross(rs, cs) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')]

# 1.8 create unitlist for all units
unitlist = row_units + column_units + square_units

# 1.9 create peers of a unit from all units
units = dict((s, [u for u in unitlist if s in u]) for s in boxes)
peers = dict((s, set(sum(units[s], [])) - set([s])) for s in boxes)

# 1.10 display function receiving "values" as a dictionary and display a 9x9 suduku board
def display(values):
    """
    Display the values as a 2-D grid.
    Input: The sudoku in dictionary form
    Output: None
    """
    width = 1 + max(len(values[s]) for s in boxes)
    line = '+'.join(['-' * (width * 3)] * 3)
    for r in rows:
        print(''.join(values[r + c].center(width) + ('|' if c in '36' else '')
                      for c in cols))
        if r in 'CF': print(line)
    return

def grid_values(grid):
    # In this function, you will take a sudoku as a string
    # and return a dictionary where the keys are the boxes,
    # for example 'A1', and the values are the digit at each
    # box (as a string) or '.' if the box has no value
    # assigned yet.

    grid = list(grid)
    for i, v in enumerate(boxes):
        if grid[i] == '.':
            grid[i] = '123456789'

    return dict(zip(boxes, grid))


# boxes.get('.') and boxes.update({'.': 'my definition'})
# print(boxes)

#3. Test function.py ----------------------------
# print(grid_values('..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..'))

#4. Test utils.py ----------------------------
# display(grid_values('..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..'))

# 2.1 implement eliminate(values)
# from utils import *
def eliminate(values):
    """Eliminate values from peers of each box with a single value.

    Go through all the boxes, and whenever there is a box with a single value,
    eliminate this value from the set of values of all its peers.

    Args:
        values: Sudoku in dictionary form.
    Returns:
        Resulting Sudoku in dictionary form after eliminating values.
    """
    for i, v in values.items():
        if len(values[i]) == 1:
            for i2 in peers[i]:
                if len(values[i2]) > 1:
                    values[i2] = values[i2].replace(values[i], '')

    return values

def only_choice(values):
    """Finalize all values that are the only choice for a unit.

    Go through all the units, and whenever there is a unit with a value
    that only fits in one box, assign the value to this box.

    Input: Sudoku in dictionary form.
    Output: Resulting Sudoku in dictionary form after filling in only choices.
    """

    for i, v in values.items():
        if len(values[i]) > 1:
            key = ''
            for i2 in units[i]:
                for i3 in i2:
                    if len(values[i3]) > 1 and i != i3:
                        key += values[i3]

            for v2 in v:
                if v2 not in key:
                    values[i] = v2
                    break
    return values


# 2. function.py ----------------------------
# 2.1 combine the functions eliminate and only_choice to write the function reduce_puzzle
# from utils import *
def reduce_puzzle(values):
    """
    Iterate eliminate() and only_choice(). If at some point, there is a box with no available values, return False.
    If the sudoku is solved, return the sudoku.
    If after an iteration of both functions, the sudoku remains the same, return the sudoku.
    Input: A sudoku in dictionary form.
    Output: The resulting sudoku in dictionary form.
    """

    new_values = eliminate(values)
    new_values = only_choice(new_values)
    new_values = eliminate(values)
    new_values = only_choice(new_values)
    new_values = eliminate(values)
    new_values = only_choice(new_values)
    new_values = eliminate(values)
    new_values = only_choice(new_values)
    new_values = eliminate(values)
    new_values = only_choice(new_values)

    return new_values


#3. Test utils.py ----------------------------
values = grid_values('..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3..')
print("The original Sudoku board is **********************************************")
display(values)

#4. Test function.py ----------------------------
new_values = reduce_puzzle(values)
print("\n")
print("After applying constrint propagaton (both eliminate and only_choice strategies)*****************")
display(new_values)